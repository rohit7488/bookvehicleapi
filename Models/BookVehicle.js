const mongoose = require('mongoose');

const BookVehicleScehma = mongoose.Schema({
    firstName : {type: String, required: true},
    lastName : {type: String, required: true},
    vehicleType : {type: String, enum:['SUV','SEDAN','HATCHBACK','CRUISER','SPORTS'], required: true},
    vehicleWheels : {type: String, enum:['TWO','FOUR'], required: true},
    vehicleModel : {type: String, required: true},
    bookingDate : {type: Date, required: true}
}, {timestamps: true})

module.exports = mongoose.model('BookVehicle', BookVehicleScehma);