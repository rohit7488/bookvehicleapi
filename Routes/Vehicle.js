const express = require('express');
const router = express.Router();
const Vehicle = require('../Models/Vehicle');
const BookVehicle = require('../Models/BookVehicle');

router.get('/all', (req,res,next)=>{
    Vehicle.find()
    .then(result => {
        if(result.length > 0) {
            res.status(200).json({
                allVehiclesList : result
            })
        } else {
            res.status(404).json({
                message : 'no vehicles found.'
            })
        }
       
    })
    .catch(error =>{
        res.status(500).json({
            errorOccured: 'something went wrong.'
        })
    });
});

router.post('/bookvehicle', (req,res,next) => {
    const bookedVehicle = new BookVehicle(req.body);
    bookedVehicle.save()
    .then(result => {
        res.status(201).json({
            message: 'new bookings reserved',
            bookedVehicle : result
        })
    })
    .catch(error => {
        res.status(500).json({
            message: 'something went wrong, try again.',
            errorOccured: error
        })
    });
})

module.exports = router