const mongoose = require('mongoose');

const VehicleSchema = mongoose.Schema({
    modelName : {type: String, required: true},
    vehicleType : {type: String, enum:['SUV','SEDAN','HATCHBACK','CRUISER','SPORTS'], required: true},
    vehicleWheels : {type: String, enum:['TWO','FOUR'], required: true}
})

module.exports = mongoose.model('Vehicle', VehicleSchema);