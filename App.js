const express = require('express');
const app = express();
const env = require('dotenv');
const mongoose = require('mongoose');
const VehicleRouter = require('./Routes/Vehicle.js');
const bodyParser = require('body-parser');
const cors = require('cors');

env.config();

app.use(cors());

app.listen(process.env.PORT,  () => {
    console.log(`server is running at port: ${process.env.PORT}`)
})

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }))

// parse application/json
app.use(bodyParser.json())

mongoose.connect(`mongodb+srv://rental:Rohit12345@cluster0.sjopp.mongodb.net/myFirstDatabase?retryWrites=true&w=majority`)
.then(console.log('connected to DB'))
.catch((error) => {
    console.log(error)
});

app.use('/vehicle', VehicleRouter);

module.exports = app