const mongoose = require('mongoose');
const Vehicle = require('../Models/Vehicle');

const vehicleData = [ 
    new Vehicle({
            modelName: 'Creta',
            vehicleType: 'SUV',
            vehicleWheels: 'FOUR'
        }),
        new Vehicle({
            modelName: 'Honda City',
            vehicleType: 'SEDAN',
            vehicleWheels: 'FOUR'
        }),
        new Vehicle({
            modelName: 'Audi Q7',
            vehicleType: 'SUV',
            vehicleWheels: 'FOUR'
        }),
        new Vehicle({
            modelName: 'i20',
            vehicleType: 'HATCHBACK',
            vehicleWheels: 'FOUR'
        }),
        new Vehicle({
            modelName: 'Ninja',
            vehicleType: 'SPORTS',
            vehicleWheels: 'TWO'
        }),
        new Vehicle({
            modelName: 'Swift',
            vehicleType: 'HATCHBACK',
            vehicleWheels: 'FOUR'
        }),
        new Vehicle({
            modelName: 'Rapid',
            vehicleType: 'SEDAN',
            vehicleWheels: 'FOUR'
        }),
        new Vehicle({
            modelName: 'Jawa',
            vehicleType: 'CRUISER',
            vehicleWheels: 'TWO'
        }),
        new Vehicle({
            modelName: 'Royal Enfield',
            vehicleType: 'CRUISER',
            vehicleWheels: 'TWO'
        }),
        new Vehicle({
            modelName: 'Harrier',
            vehicleType: 'SUV',
            vehicleWheels: 'FOUR'
        }),
        new Vehicle({
            modelName: 'Superb',
            vehicleType: 'SEDAN',
            vehicleWheels: 'FOUR'
        }),
        new Vehicle({
            modelName: 'Yamaha',
            vehicleType: 'SPORTS',
            vehicleWheels: 'TWO'
        }), 
]

 mongoose.connect(`mongodb+srv://rental:Rohit12345@cluster0.sjopp.mongodb.net/myFirstDatabase?retryWrites=true&w=majority`)
.then(() => console.log('connected to db in development enviornment'))
.catch((error) => {
    console.log(error);
    process.exit(1);
});

vehicleData.map(async (v, index)=> {
    await v.save((err, result) => {
        if(index === vehicleData.length - 1){
            console.log('DONE');
            mongoose.disconnect();
        }
    })
})